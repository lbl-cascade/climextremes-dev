## Overview

[![CRAN](http://www.r-pkg.org/badges/version/climextRemes)](https://cran.r-project.org/web/packages/climextRemes)
[![PyPI version](https://badge.fury.io/py/climextremes.svg)](https://badge.fury.io/py/climextremes)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3240582.svg)](https://doi.org/10.5281/zenodo.3240582)

Repository for the climextRemes R and Python package for extreme value analysis of climate data, including generalized extreme value and peaks-over-threshold (using the point process approach) models, as well as tools for estimating risk ratios with uncertainty.

Many of the methods are discussed in Paciorek, C.J., D.A. Stone and M.F. Wehner. 2018. Quantifying statistical uncertainty in the attribution of human influence on severe weather. Weather and Climate Extremes 20: 69-80. doi: 10.1016/j.wace.2018.01.002, as well as the corresponding arXiv article: http://arxiv.org/abs/1706.03388.

Thanks to Hari Krishnan of Lawrence Berkeley National Laboratory for extensive help in porting the package for use in Python.

Development of this software was supported by the Director, Office of
Science, Office of Biological and Environmental Research of the
U.S. Department of Energy under Contract No. DE-AC02-05CH11231 as part
of their Regional & Global Climate Modeling (RGCM) Program within the
Calibrated And Systematic Characterization Attribution and Detection
of Extremes Scientific Focus Area (CASCADE SFA).

## Documentation

In R, please see:

```
library(help = climextRemes) # for an overview
help(climextRemes::fit_pot)  # for an individual function
```

In Python, please see:
```
help(climextremes.fit_pot)  # for an individual function
```

The full set of help information for the Python version can be found in this repository at `climextRemes/inst/python_wrapper/docs/climextremesDocumentation.pdf`.

## Installation from standard R and Python repositories


climextRemes is distributed as an R package through CRAN and as a Python package through PyPI (i.e., installable via `pip`) and for the Anaconda distribution of Python through the 'cascade' channel. The Python package merely wraps the core functionality in the R package.

#### Installation of the R package from CRAN

From within R, run

```
install.packages('climextRemes')
```

#### Installation of the Python package via pip

First make sure you have R installed. Then:

```
pip install climextremes
```

Note that you should not have to install the R `climextRemes` package, as when you first do `import climextremes` in Python, it should call out to R to install the R `climextRemes` package and its dependencies. However if that fails for some reason, you might try installing `climextRemes` directly in R as shown above.

Note that the Python package is 'climextremes' not 'climextRemes' (note the lower case 'r').


#### Installation of the Python package via Conda

Note that if you don't have R installed, Conda will install it for you. 

```
conda install -c conda-forge -c cascade climextremes
```

Alternatively, we recommend the use of `mamba` in place of `conda` to avoid potentially
very slow dependency resolution by `conda`:

```
conda install -c conda-forge mamba
mamba install -c conda-forge -c cascade climextremes
```

Note that you should not have to install the R `climextRemes` package,
as when you first do `import climextremes` in Python, it should call
out to R to install the R `climextRemes` package and its
dependencies. However if that fails for some reason, you might try
installing `climextRemes` directly in R as shown above.

On M1/M2 Macs (i.e., Apple Silicon) you may encounter an error when the `Lmoments` R dependency is installed automatically for you, with an error message like "no member named 'Rlog1p' in namespace 'std'". In that case, in the shell before starting Python, running `PKG_CPPFLAGS="-DHAVE_WORKING_LOG1P"` should fix the problem.

The use of the `conda-forge` channel is because the default
version of the `rpy2` package, on which `climextremes` depends, in the default
anaconda channel is quite old and would force use of a very old version of 
`pandas`. Use of the `conda-forge` channel allows use of newer versions of 
`rpy2` and `pandas`. In addition use of `conda-forge` avoids some other 
dependency issues.

Finally with Python 3.7, you may need to install `typing-extensions` and in Python 3.8, you may need to install `pytest`. These are dependencies of `rpy2` in these versions of Python that are not being automatically installed for some reason.

Finally, note that the Python package is 'climextremes' not 'climextRemes' (note the lower case 'r').

## Installation from this repository

While we recommend installing via pip or conda, to install climextRemes from this repository, follow these instructions.

#### R

```
VERSION=0.3.1 # modify as needed
Rscript -e "install.packages(c('extRemes', 'boot'))"
git clone https://bitbucket.org/lbl-cascade/climextRemes-dev
R CMD build climextRemes
# install to default package location
R CMD INSTALL climextRemes_${VERSION}.tar.gz
# alternatively, install to user-specified location
R CMD INSTALL --library=/tmp/foo climextRemes_${VERSION}.tar.gz
```

#### Python

First install the R package, either from CRAN or per instructions above.

These instructions should work on in a Linux or MacOS terminal; Windows users may need to make some modifications.

```
pip install rpy2 numpy pandas tzlocal
git clone https://bitbucket.org/lbl-cascade/climextRemes-dev/climextRemes
cd climextRemes/climextRemes/inst/python_wrapper

# install to default package location:
python setup.py install

# alternatively, install to user-specified location:
INSTALLDIR=/tmp/foo
PYTHONVERSION=python3.10  # modify as needed
mkdir -p ${INSTALLDIR}/lib/${PYTHONVERSION}/site-packages
export PYTHONPATH=${INSTALLDIR}/lib/${PYTHONVERSION}/site-packages:${PYTHONPATH}
python setup.py install --prefix=${INSTALLDIR}
```

## License

Licensed under a modified BSD license by the University of California; please see climextRemes/license.

## Help and issues

For help or to report issues, please email Chris Paciorek at paciorek AT berkeley.edu.

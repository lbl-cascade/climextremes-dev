.PHONY: FORCE

VERSION=$(shell grep Version: climextRemes/DESCRIPTION | grep -o '[^ ]*$$')

man: FORCE
	cd climextRemes; Rscript -e "library(roxygen2); roxygenize()"

version: FORCE
	@echo $(VERSION)

climextRemes_$(VERSION).tar.gz: FORCE
	@echo "Building climextRemes package in this directory"
	(R CMD build climextRemes)

build: climextRemes_$(VERSION).tar.gz

install: climextRemes_$(VERSION).tar.gz
	R CMD INSTALL climextRemes_$(VERSION).tar.gz

check: build FORCE
	R CMD check --as-cran climextRemes_$(VERSION).tar.gz

test: FORCE
	cd climextRemes; Rscript -e "library(climextRemes); library(testthat); test_package('climextRemes')"

# Keeps .Rproj.user/ and .Rhistory.
clean: FORCE
	git clean -dfx -e .R*

mrproper: FORCE
	git clean -dfx

FORCE:


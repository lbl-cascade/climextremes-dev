PYVER=3.11
CEVER=0.3.1rc1
CEVER_R=0.3.1

# variations:
# Python 3.7, 3.8, 3.9, 3.10
# R 4.2
# rpy2 2.9.4, 3.0.0, 3.3.5, 3.5.3
# conda, pip, conda+conda-R
# w/ R climextRemes missing, 0.2.1, 0.2.2
# w/ and w/o extRemes, distillery, Lmoments
# w/ and w/o rpy2, numpy, pandas


### test PyPI: do on Linux and Linux under 3.8 (don't have virtualenv readily accessible on Mac)

Rscript -e "remove.packages(c('extRemes','climextRemes','distillery','Lmoments'))"
R CMD INSTALL climextRemes_${CEVER_R}.tar.gz  # otherwise 'import climextremes' will pull in old CRAN version

## do in a conda environment if needed to try different python versions

## use a virtualenv if needed to isolate
VE=~/Desktop/ce_${CEVER}_${PYVER}
virtualenv ${VE} 
source ${VE}/bin/activate

pip install rpy2 numpy pandas tzlocal # needed because not in the index-url, presumably
pip install requests # for SCF R pkg logger o.w. have issues with accessing climextRemes
pip install --index-url https://test.pypi.org/simple/ climextremes==${CEVER}

## now test:

python ~/Desktop/test.py

## try alternative rpy2 versions and run the tests
pip install rpy2==3.2.5
pip install rpy2==2.9.4 pandas==0.25.3

### Conda from paciorek channel: do on Linux and Mac

CHANNEL=paciorek

mamba create -y --name ce_${CEVER}_${PYVER} python=${PYVER}
source activate ce_${CEVER}_${PYVER}

## try with anaconda (probably won't work)
conda install -y -c ${CHANNEL} climextremes=${CEVER}
#  - feature:/linux-64::__glibc==2.31=0
#  - python=3.9 -> libgcc-ng[version='>=7.5.0'] -> __glibc[version='>=2.17']


## try with conda-forge (probably will take forever to resolve)
conda install -y -c conda-forge -c ${CHANNEL} climextremes=${CEVER}

## should work with mamba
# conda install -y -c conda-forge mamba
mamba install -y -c conda-forge -c ${CHANNEL} climextremes=${CEVER}

## this will use old R climextRemes
python ~/Desktop/test.py

## now try with updated climextRemes

R CMD INSTALL climextRemes_${CEVER_R}.tar.gz

python ~/Desktop/test.py

### regular PyPI, using rc1

VE=~/Desktop/ce_${CEVER}_${PYVER}
virtualenv ${VE}
source ${VE}/bin/activate

pip install climextremes==${CEVER}

### Conda from cascade channel, using rc1

CHANNEL=cascade

mamba create -y --name ce_${CEVER}_${PYVER} python=${PYVER}
source activate ce_${CEVER}_${PYVER}

## Try, but will probably hang.
conda install -y -c conda-forge -c ${CHANNEL} climextremes=${CEVER}

conda install -y -c conda-forge mamba
mamba install -y -c conda-forge -c ${CHANNEL} climextremes=${CEVER}

## this will use old R climextRemes
python ~/Desktop/test.py

## manually install new R climextRemes and test again
